
public class Temparaturtabelle {
	
	
	public static void main(String[] args) {
		
		double fahrenheitA = -20;
		double fahrenheitB = -10;
		double fahrenheitC = 0;
		double fahrenheitD = 20;
		double fahrenheitE = 30;
		
		double celsiusA = -28.8889;
		double celsiusB = -23.3333;
		double celsiusC = -17.7778;
		double celsiusD = -6.6667;
		double celsiusE = -1.1111;
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s\n", "Celsius");
		System.out.println("-----------------------");
		
		
		System.out.printf("%-12.2f", fahrenheitA);
		System.out.print("|");
		System.out.printf("%10.2f\n", celsiusA);
		
		
		System.out.printf("%-12.2f", fahrenheitB);
		System.out.print("|");
		System.out.printf("%10.2f\n", celsiusB);
		
		System.out.printf("%+-12.2f", fahrenheitC);
		System.out.print("|");
		System.out.printf("%10.2f\n", celsiusC);
		
		System.out.printf("%+-12.2f", fahrenheitD);
		System.out.print("|");
		System.out.printf("%10.2f\n", celsiusD);
		
		System.out.printf("%+-12.2f", fahrenheitE);
		System.out.print("|");
		System.out.printf("%10.2f\n", celsiusE);
		
		
	}
	

}
