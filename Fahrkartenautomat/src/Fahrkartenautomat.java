import java.util.Scanner;

class Fahrkartenautomat
{
	
	static Scanner tastatur = new Scanner(System.in); 
    static float zuZahlenderBetrag; 
    static float eingezahlterGesamtbetrag;
    static float eingeworfeneMünze;
    static float rückgabebetrag;
    static int anzahlTickets;
    static int eingabe;

    static float[] price = new float[] {0.0f,2.9f,8.6f,3.3f};
    
    public static void fahrkartenBestellungErfassen() {
    	System.out.print("Ihre Wahl:");
        eingabe = tastatur.nextInt();
    	return;
    }
    
    public static void anzahlTicketsFestlegen() {
    	System.out.print("Anzahl Tickets:");
        anzahlTickets = tastatur.nextInt();
        
        if(anzahlTickets > 10) {
        	anzahlTicketsFestlegen();
        }
        
        else if (anzahlTickets < 1) {
        	anzahlTicketsFestlegen();
        }
        
        else{
            if(eingabe != 3) {
                zuZahlenderBetrag = price[eingabe] * anzahlTickets;
            }
            else {
                System.out.println("Wie viele Personen?");
                int eingabe2 = tastatur.nextInt();
                zuZahlenderBetrag = price[eingabe] * eingabe2 * anzahlTickets;
            }
        }
    }
    public static float fahrkartenBezahlen(float zuZahlenderBetrag) {
    	
    	eingezahlterGesamtbetrag = (float) 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s %.2f %s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag) , "Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextFloat();
     	   if (eingeworfeneMünze > 2 || eingeworfeneMünze < 0.05) {
     	       System.out.println("Zu kleine oder zu große Eingabe!");
           }
     	   else {
               eingezahlterGesamtbetrag += eingeworfeneMünze;
           }
        }
        
        rückgabebetrag = eingezahlterGesamtbetrag - (zuZahlenderBetrag);
    	
    	return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    	return;
    }
    
    public static void rueckgeldAusgeben(float rückgabebetrag) {
    	
    	if(rückgabebetrag >= 0.0)
        {
     	   System.out.printf("%s %.2f %s\n" , "Der Rückgabebetrag in Höhe von " , rückgabebetrag , " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    	return;
    }
    
    public static void main(String[] args)
    {

      while(true) {
          System.out.println("Fahrkartenbestellvorgang:\n===========================");
          System.out.println("Wählen Sie ihre Wunschfahrkarte aus:\n Einzelfahrscheine (1)\n Tageskarten (2)\n Gruppenkarten (3)");
          fahrkartenBestellungErfassen();
          anzahlTicketsFestlegen();
          fahrkartenBezahlen(zuZahlenderBetrag);
          fahrkartenAusgeben();
          rueckgeldAusgeben(rückgabebetrag);


          System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                  "vor Fahrtantritt entwerten zu lassen!\n" +
                  "Wir wünschen Ihnen eine gute Fahrt.");
          System.out.println("\n\n\n");
      }
    }
}